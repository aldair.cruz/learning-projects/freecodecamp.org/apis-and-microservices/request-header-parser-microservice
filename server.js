// server.js
// where your node app starts

// init project
const express = require('express');
const app = express();
const genericRequestMiddleware = require('./ genericRequestMiddleware');

// http://expressjs.com/en/4x/api.html#app.settings.table
// http://expressjs.com/en/api.html#req.ip
app.disable('trust proxy');

// enable CORS (https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
// so that your API is remotely testable by FCC 
var cors = require('cors');
app.use(cors({optionSuccessStatus: 200}));  // some legacy browsers choke on 204

// http://expressjs.com/en/starter/static-files.html
app.use(express.static('public'));

// http://expressjs.com/en/starter/basic-routing.html
app.get("/", function (req, res) {
  res.sendFile(__dirname + '/views/index.html');
});


// your first API endpoint... 
app.get("/api/whoami", function (req, res) {

  res.json({
    "ipaddress": getForwardForClient(req),
    "language": req.get('Accept-Language'),
    "software": req.get('User-Agent')
  });

});

genericRequestMiddleware.use(app);


// listen for requests :)
var listener = app.listen(process.env.PORT, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});

function getForwardForClient(req) {
  
  if (!req) throw "Bad request";

  const forwardedForString = req.get('X-Forwarded-For');
  const forwardForArray = forwardedForString && forwardedForString.length ? forwardedForString.split(',') : [];
  return forwardForArray.length ? forwardForArray[0] : req.ip;

}
